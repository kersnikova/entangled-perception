import processing.serial.*; //ukaz za prevajalnik --> gre v drugi file pogledat za kodo (import knjižnice)

Serial myPort;        // The serial port (deklaracija spremenljivke --> serial je tip spremenljivke ki se nahaja v knjižnici - ni enostavna spremenljivka ampak objekt, s funkcijami ki jih lahko kličeš)
int xPos = 1;         // horizontal position of the graph (int = celo število)
float height_old = 0;  //deklaracije spremenljivk
float height_new = 0;
float inByte = 0;





void setup () { //void - tip funkcije ki ne vrača nič; setup - nastavitev, se izvede enkrat na začetku

  
  size(1000, 400);   // set the window size:     

  // List all the available serial ports
  println(Serial.list()); //Serial.list - "statična funkcija" pokliče list, ki je v knjižnici (dinamične funkcije so take, ki jih kot objekt definiramo na začetku ex. Serial myPort)
   

  myPort = new Serial(this, Serial.list()[Serial.list().length-1], 9600);// Open whatever port is the one you're using. Baud rate - hitrost komunikacije med Processingom in Arduinom


  myPort.bufferUntil('\n');   // don't generate a serialEvent() unless you get a newline character:
  // set inital background:
  background(0xff);
}


void draw () { //funkcija ki se ponavlja (loop)
  // everything happens in the serialEvent()
  line(xPos - 1, height_old, xPos, height_new);
  height_old = height_new;
  if (xPos >= width) {  // at the edge of the screen, go back to the beginning:
        xPos = 0;
        background(0xff);
  }

}


void serialEvent (Serial myPort) {
  println("entered serialEvent"); //<>//
  

  String inString = myPort.readStringUntil('\n');// get the ASCII string: //<>//
  
  if (inString != null) {
    
    inString = trim(inString);// trim off any whitespace:
    // If leads off detection is true notify with blue line
    if (inString.equals("!")) { 
      stroke(0, 0, 0xff); //Set stroke to blue ( R, G, B)
      inByte = 512;  // middle of the ADC range (Flat Line)
    }
    // If the data is good let it through
    else {
      stroke(0xff, 0, 0); //Set stroke to red ( R, G, B)
      inByte = float(inString); 
     }
     println(inByte);
     //Map and draw the line for new data point

     inByte = map(inByte, 200, 480, 0, height);  //map preslika prvi parameter iz prve (2 in 3 podatek --> ta dva spreminjaš, da približaš/oddaljiš graf) v drugo zalogo vrednost (3 in 4 podatek)
     height_new = height - inByte;  //vrednost moraš odštevat od višine, ker je 0 na vrhu zaslona (v računalniški grafiki je izhodišče vedno levo zgoraj)
     
     
    
     

     xPos++;

  }
}
